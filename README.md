ShardingSphere-example
=======================

- [github地址](https://github.com/apache/shardingsphere) ：https://github.com/apache/shardingsphere
- [官方文档地址](https://shardingsphere.apache.org) ：https://shardingsphere.apache.org

## 关于分支
master 分支是自己编写的测试案例，其余分支才是官方示例，请根据需要检出代码。

## project structure
```
shardingsphere-example
  ├── shardingsphere-jdbc-example-generator
  ├── shardingsphere-parser-example
  └── src/resources
          └── manual_schema.sql
```

## Available Examples
| Example                                                                         | Description                                                                    |
|---------------------------------------------------------------------------------|--------------------------------------------------------------------------------|
| [ShardingSphere-JDBC Examples](shardingsphere-jdbc-example-generator/README.md) | Generate the examples by configuration and show how to use ShardingSphere-JDBC |

